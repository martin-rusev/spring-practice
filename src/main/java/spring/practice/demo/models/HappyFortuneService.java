package spring.practice.demo.models;

import org.springframework.stereotype.Component;
import spring.practice.demo.models.contracts.FortuneService;

@Component
public class HappyFortuneService implements FortuneService {

    @Override
    public String getFortune() {
        return "You get lucky today!";
    }
}
