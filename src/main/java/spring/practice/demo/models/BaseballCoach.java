package spring.practice.demo.models;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import spring.practice.demo.models.contracts.Coach;
import spring.practice.demo.models.contracts.FortuneService;

@Component
public class BaseballCoach implements Coach {
    @Autowired
    @Qualifier("restFortuneService")
    private FortuneService fortuneService;

    public BaseballCoach() {
        System.out.println("Empty constructor was invoked");
    }

    @Override
    public String getDailyTraining() {
        return "30 minutes training";
    }

    @Override
    public String getDailyFortune() {
        return fortuneService.getFortune();
    }

}
