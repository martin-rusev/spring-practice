package spring.practice.demo.models;

import org.springframework.stereotype.Component;
import spring.practice.demo.models.contracts.FortuneService;

@Component
public class RestFortuneService implements FortuneService {
    @Override
    public String getFortune() {
        return "This is sad fortune service!";
    }
}
