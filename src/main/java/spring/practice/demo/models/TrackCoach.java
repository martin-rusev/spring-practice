package spring.practice.demo.models;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import spring.practice.demo.models.contracts.Coach;
import spring.practice.demo.models.contracts.FortuneService;

@Component
public class TrackCoach implements Coach {
    private final FortuneService fortuneService;

    @Autowired
    public TrackCoach(@Qualifier("restFortuneService") FortuneService fortuneService) {
        this.fortuneService = fortuneService;
    }

    @Override
    public String getDailyTraining() {
        return "2 hours training and 5 minutes rest";
    }

    @Override
    public String getDailyFortune() {
        return "TrackCoach: " + fortuneService.getFortune();
    }

    private void postConstruct() {
        System.out.println("Initialise method called on TrackCoach bean");
    }

    private void preDestroy() {
        System.out.println("Pre-destroy method called on TrackCoach bean");
    }

}
