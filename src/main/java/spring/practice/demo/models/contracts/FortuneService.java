package spring.practice.demo.models.contracts;

public interface FortuneService {
    String getFortune();
}
