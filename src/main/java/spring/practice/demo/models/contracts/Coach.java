package spring.practice.demo.models.contracts;

public interface Coach {

    String getDailyTraining();

    String getDailyFortune();
}
