package spring.practice.demo.models;

import org.springframework.beans.factory.annotation.Value;
import spring.practice.demo.models.contracts.Coach;
import spring.practice.demo.models.contracts.FortuneService;

public class SwimCoach implements Coach {
    private FortuneService fortuneService;
    @Value("${swimCoach.assistants}")
    private int assistants;
    @Value("${swimCoach.company}")
    private String company;

    public SwimCoach(FortuneService fortuneService) {
        System.out.println("SwimCoach constructor called!");
        this.fortuneService = fortuneService;
    }

    @Override
    public String getDailyTraining() {
        return "Swim daily training!";
    }

    @Override
    public String getDailyFortune() {
        return fortuneService.getFortune();
    }

    public int getAssistants() {
        return assistants;
    }

    public String getCompany() {
        return company;
    }
}
