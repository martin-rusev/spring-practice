package spring.practice.demo.models;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import spring.practice.demo.models.contracts.Coach;
import spring.practice.demo.models.contracts.FortuneService;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
@Scope("singleton")
public class SetterCoach implements Coach {
    private FortuneService fortuneService;
    private String email;
    private String team;
    private int assistants;

    public SetterCoach() {
        System.out.println("SetterCoach no - arg constructor called!");
    }

    @Override
    public String getDailyTraining() {
        return "Daily Setter Training";
    }

    @Override
    public String getDailyFortune() {
        return "Setter Injection: " + fortuneService.getFortune();
    }

    @Autowired
    @Qualifier("happyFortuneService")
    public void setFortuneService(FortuneService fortuneService) {
        System.out.println("Setter method invoked for setting FortuneService dependency");
        this.fortuneService = fortuneService;
    }

    @PostConstruct
    public void initialise() {
        System.out.println("Initialisation logic here ...");
    }

    @PreDestroy
    public void preDestroy() {
        System.out.println("Clean up logic here ...");
    }

    public void setEmail(String email) {
        System.out.println("Setter method invoked for setting email property");
        this.email = email;
    }

    public void setTeam(String team) {
        System.out.println("Setter method invoked for setting team property");
        this.team = team;
    }

    public void setAssistants(int assistants) {
        System.out.println("Setter method invoked for setting assistants property");
        this.assistants = assistants;
    }

    public String getEmail() {
        return email;
    }

    public String getTeam() {
        return team;
    }

    public int getAssistants() {
        return assistants;
    }
}
