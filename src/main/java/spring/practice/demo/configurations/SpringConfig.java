package spring.practice.demo.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import spring.practice.demo.models.RestFortuneService;
import spring.practice.demo.models.SwimCoach;
import spring.practice.demo.models.contracts.FortuneService;

@Configuration
@PropertySource("sport.properties")
public class SpringConfig {

    @Bean
    public FortuneService fortuneService() {
        return new RestFortuneService();
    }

    @Bean
    public SwimCoach swimCoach() {
        SwimCoach swimCoach = new SwimCoach(fortuneService());
        return swimCoach;
    }
}
