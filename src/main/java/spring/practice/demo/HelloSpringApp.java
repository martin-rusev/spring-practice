package spring.practice.demo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import spring.practice.demo.configurations.SpringConfig;
import spring.practice.demo.models.SwimCoach;

public class HelloSpringApp {

    public static void main(String[] args) {

        AnnotationConfigApplicationContext applicationContext =
                new AnnotationConfigApplicationContext(SpringConfig.class);

        SwimCoach coach = applicationContext.getBean("swimCoach", SwimCoach.class);

        System.out.println(coach.getCompany() + System.lineSeparator() + coach.getAssistants());

        applicationContext.close();
    }
}
